
.. include:: /Includes.rst.txt

.. _changelog:

==========
Change log
==========

Version 11.4.10
--------------

- new version
- todo: change flexform option from checkboxes to radio, thanks to @stephan_bauer


Version 11.4.8
--------------

- new version
- bugfix: see https://gitlab.com/bartista87/gridtocontainer/-/issues/23, thanks to @linawolf

Version 11.4.7
--------------

- new version
- bugfix: fix exception, thanks to @georgringer

Version 11.4.6
--------------

- new version
- small bugfix: disabled backend.js in FE


Version 11.4.5
--------------

- new version
- bugfix: fix migrateall function with numeric tx_gridelements_backend_layout keys

Version 11.4.4
--------------

- new version
- bugfix: multilanguage connected mode, thanks to @nodeadman

Version 11.4.3
--------------

- new version
- bugfix: locallang, thanks to @ayacoo

Version 11.4.2
--------------

- new version
- bugfix: change documentation

Version 11.4.1
--------------

- new version
- bugfix: change documentation

Version 11.4.0
--------------

- new version
- feature: add cli command for the migrateall-function
- change the documentation

Version 11.3.0
--------------

- new version
- feature: individual flexform migration per element for the migration per key

Version 11.2.0
--------------

- new version
- add option to migrate old gridelements-flexform value, change doc, prepare for migration via cli

Version 11.1.6
--------------

- new version
- move templates and remove ts, ts is no longer required

Version 11.1.5
--------------

- new version
- bufgix: MultipleColumnIds , see https://gitlab.com/bartista87/gridtocontainer/-/merge_requests/19, thanks to @vc-sprotte

Version 11.1.4
--------------

- new version
- bugfix: Error 404 - Backend.js , see https://gitlab.com/bartista87/gridtocontainer/-/merge_requests/18, thanks to @schmidtwebmedia

Version 11.1.3
--------------

- new version
- small template fix, see https://gitlab.com/bartista87/gridtocontainer/-/merge_requests/16, thanks to @schmidtwebmedia

Version 11.1.2
--------------

- new version
- bugfix in composer.json

Version 11.1.1
--------------

- new version
- see issue: https://gitlab.com/bartista87/gridtocontainer/-/issues/11

Version 11.1.0
--------------

- new version
- change documentation

Version 11.0.6
--------------

- bugfix sameCid = 0
- new feature flexform support

Version 11.0.5
--------------

- small fix in composer.json for documantation, thanks to  @linawolf
- small fix in readme.rst for documantation, thanks to  @linawolf

Version 11.0.4
--------------

- adds correct documentation, thanks to  @linawolf
- fix: remove empty check on 'columnid' which results always in 0, thanks to @anjey.link


Version 11.0.3
--------------

- small fixes on ext_emconf.php
- small fixes on composer.json


Version 11.0.2
--------------

- migrate to typo3 v11 and refactor
- bugfix in migrategeneral-Action
- migrate backend templates and css to bootstrap 5
- include bugfix "Columns not migrate, when based on container with empty column."
- version to only typo3 v11

Thanks to the contributers: @stigfaerch and @florian.obwegs
