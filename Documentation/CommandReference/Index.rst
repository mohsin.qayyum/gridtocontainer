
.. include:: /Includes.rst.txt

.. _command-reference:

===========
Command Reference
===========

The following reference was automatically generated from code.

Application Options
-------------------

The following options can be used with every command:

`-h, --help`
		Display help for the given command. When no command is given display help for the list command
`-q, --quiet`
		Do not output any message
`-V, --version`
		Display this application version
`--ansi|--no-ansi`
		Force (or disable --no-ansi) ANSI output
`-n, --no-interaction`
		Do not ask any interactive question
`-v|vv|vvv, --verbose`
		Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

**Display help for the command**

The `-h` command displays help for the command:


.. code-block:: shell

   vendor/bin/typo3 gridtocontainer:migrateall -h


Required arguments
=========

`grididentifier`
		Gridelements identifier to migrate all the elements from this type
`containeridentifier`
		The new EXT:container element-identifier e.g. ce_columns2
`flexformidentifier`
		If you want a clean flexform field, write "clean". If you want a flexform value from the TCA than write the identifier or if you want the old flexform value than write "old".
`oldcolumids`
		The old Column-ID/s, separated with a commar without space
`columnids`
		New Column-ID/s, separated with a commar without space. It must be used at the end of the argument list and it must have the same order as the old columids
